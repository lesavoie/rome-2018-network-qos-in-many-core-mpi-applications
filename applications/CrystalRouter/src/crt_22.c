#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "c99.h"
#include "name.h"
#include "fail.h"
#include "types.h"
#include "comm.h"
#include "mem.h"
#include "crystal.h"
#include "sort.h"
#include "sarray_sort.h"

int main(int narg, char *arg[])
{
  comm_ext world; int np;
  struct comm comm;
  struct crystal cr;
  uint i, *data;

  struct test { uint f; uint modf; uint c; };
  buffer buf={0,0,0};
  uint count,j,j0;
  double r, ttime;

#ifdef MPI
  MPI_Init(&narg,&arg);
  world = MPI_COMM_WORLD;
  MPI_Comm_size(world,&np);
#else
  world=0, np=1;
#endif

  comm_init(&comm,world);
  
  crystal_init(&cr,&comm);                      //copies comm and initialized arrays of cr

  uint size  = 10;                            //number of messages per proc
  struct test work1[size];

  uint niter = 100;                             //number of iterations/tests
  srand(time(0)+comm.id);                       //seed random number

  double tstart=comm_time();                    //start clock
  for(int n=1;n<=niter;++n) {

    for(i=0;i<size;++i)  {
          r = rand() * (1.0/(RAND_MAX+1.0));
          work1[i].f    =r*99999;               //random integer bound 
          work1[i].modf =work1[i].f % comm.np;  //mod of work1=target proc
      }

    sarray_sort(struct test,work1,size,modf,0,&buf);
  
    count = 0, j = 0, j0 = 0;                   //count # to send to proc 
    for(i=0;i<size;++i){
       j=work1[i].modf;
       if(j==j0){ 
         count += 1; 
         work1[i].c=count;
       }
  
       if(j!=j0) { 
          work1[i-count].c=count;
          count  = 1, j0=j;
       }
    }
    work1[i-count].c=count;

    cr.data.n = size+(3*comm.np);                 //number of integers in cr
    buffer_reserve(&cr.data,cr.data.n*sizeof(uint)); 
    data = cr.data.ptr;                           //starting address
    for(j=0,i=0;i<comm.np;++i, data+=3+data[2]) {    
      if(j>=size) {                               //for proc not sending
         data[0] = 0;
         data[1] = comm.id;
         data[2] = 0;
      }else {
        data[0] = work1[j].modf;
        data[1] = comm.id;
        data[2] = work1[j].c;
      for(int k=3; k<data[2]+3;++k,j++)
         data[k]=work1[j].f;
      }
    }

    crystal_router(&cr);
    crystal_router(&cr);
  }
  double tstop=comm_time();                       //end clock
  ttime = (tstop - tstart) ;
  ttime = ttime/(double)niter;                    //divid by iterations
 
  if(comm.id==0) printf("Time elapsed: %f\n",ttime);  

  crystal_free(&cr);
  buffer_free(&buf);
  comm_free(&comm);

//diagnostic("",__FILE__,__LINE__,
//  "test successful %u/%u",(unsigned)comm.id,(unsigned)comm.np);
  
#ifdef MPI
  MPI_Finalize();
#endif

  return 0;
}
