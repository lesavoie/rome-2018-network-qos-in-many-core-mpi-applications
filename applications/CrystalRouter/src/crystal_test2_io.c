#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "c99.h"
#include "name.h"
#include "fail.h"
#include "types.h"
#include "comm.h"
#include "mem.h"
#include "crystal.h"
#include "sort.h"
#include "sarray_sort.h"
#include "sarray_transfer.h"

int main(int narg, char *arg[])
{
  comm_ext world; int np;
  struct comm comm;
  struct crystal cr;
  uint i;

  struct test { uint f; uint modf; uint c; };
  struct test *work1;
  buffer buf={0,0,0};
  struct array A;
  double r, ttime;

#ifdef MPI
  MPI_Init(&narg,&arg);
  world = MPI_COMM_WORLD;
  MPI_Comm_size(world,&np);
#else
  world=0, np=1;
#endif

  comm_init(&comm,world);
  
  crystal_init(&cr,&comm);                      //copies comm and initialized arrays of cr

  uint size  = 10;                              //number of messages per proc
  array_init(struct test,&A,size); 
  A.n   = size;
  work1 = A.ptr;

  uint niter = 1;                               //number of iterations/tests
  srand(time(0)+comm.id);                       //seed random number

  double tstart=comm_time();                    //start clock
  for(int n=1;n<=niter;++n) {

    for(i=0;i<A.n;++i)  {
          r = rand() * (1.0/(RAND_MAX+1.0));
          work1[i].f    =r*99999;               //random integer bound 
          work1[i].modf =work1[i].f % comm.np;  //mod of work1=target proc
      }

    sarray_sort(struct test,work1,A.n,modf,0,&buf);
   for(i=0;i<A.n;++i)
    printf("%02d send -> %02d: %08d\n",comm.id,work1[i].modf,work1[i].f);

    sarray_transfer(struct test,&A,modf,1,&cr);
    sarray_transfer(struct test,&A,modf,1,&cr);

   work1=A.ptr;
   for(i=0;i<A.n;++i)
    printf("%02d GOT -> %02d: %08d \n",
      (int)comm.id,(int)work1[i].modf,(int)work1[i].f);

  }
  double tstop=comm_time();                       //end clock
  ttime = (tstop - tstart) ;
  ttime = ttime/(double)niter;                    //divid by iterations
  ttime = ttime/2.0;                    //divid by iterations
 
  if(comm.id==0) printf("Time elapsed: %f\n",ttime);  

  array_free(&A);
  crystal_free(&cr);
  buffer_free(&buf);
  comm_free(&comm);

//diagnostic("",__FILE__,__LINE__,
//  "test successful %u/%u",(unsigned)comm.id,(unsigned)comm.np);
  
#ifdef MPI
  MPI_Finalize();
#endif

  return 0;
}
