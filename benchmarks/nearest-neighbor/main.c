/*
    Copyright (C) 2018  Staci Smith, Lee Savoie

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/*
 * Simple 2D nearest neighbor benchmark
 *
 * To do:
 * -Time only the communication loop
 */


extern void do_sleep(long sleep);
extern void print_hosts();

#ifdef PROFILE
extern void initProfile(int numPhases);
extern void startComp();
extern void endComp();
extern void startCommExchange();
extern void endCommExchange();
extern void enterSync();
extern void exitSync(int rank);
extern void writeProfile(int rank, int numRanks);
#endif

#define _GNU_SOURCE
#include <sched.h>
#include <string.h>

#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"

#ifdef SCOREP_USER_ENABLE
#include <scorep/SCOREP_User.h>
#endif

#define TAG 0

void postRecv(int *, int, int, MPI_Request [], int *);
void postSend(int *, int, int, MPI_Request [], int *);

MPI_Comm appWorld;

int nn_main(int argc, char ** argv, MPI_Comm comm) {

appWorld = comm;

int rank, ranks;
MPI_Comm_size(appWorld, &ranks);
MPI_Comm_rank(appWorld, &rank);

if (argc != 6) {
  if (rank == 0) printf("Usage: %s <row size> <message size> <number of timesteps> <comm iters> <sleep nanoseconds>\n", argv[0]);
  MPI_Abort(appWorld, 1);
}

int rowSize, msgSize, numPhases, iterations;
long sleep;
rowSize = atoi(argv[1]);
msgSize = atoi(argv[2]);
numPhases = atoi(argv[3]);
iterations = atoi(argv[4]);
sleep = atol(argv[5]);

if (rank == 0) {
  printf("Row size: %d, message size: %d, timesteps: %d, commiters: %d, computation: %ld nano\n", rowSize, msgSize, numPhases, iterations, sleep);
  fflush(stdout);
}

// Calculate the size of the columns
if (ranks % rowSize != 0) {
  if (rank == 0) printf("Row size must divide evenly into the number of ranks.\n");
  MPI_Abort(appWorld, 2);
}
int colSize = ranks / rowSize;

// Find my neighbors
int myX, myY;
int leftN, topN, rightN, bottomN, neighbors;
leftN = topN = rightN = bottomN = -1;
neighbors = 0;
myX = rank % rowSize;
myY = rank / rowSize;
if (myX > 0) {
  leftN = rank - 1;
  ++neighbors;
}
if (myX < rowSize - 1) {
  rightN = rank + 1;
  ++neighbors;
}
if (myY > 0) {
  topN = rank - rowSize;
  ++neighbors;
}
if (myY < colSize - 1) {
  bottomN = rank + rowSize;
  ++neighbors;
}

// Communicate with neighbors
int leftSendBuf[msgSize], topSendBuf[msgSize], rightSendBuf[msgSize], bottomSendBuf[msgSize];
int leftRecvBuf[msgSize], topRecvBuf[msgSize], rightRecvBuf[msgSize], bottomRecvBuf[msgSize];
MPI_Request requests[neighbors * 2];

#ifdef PROFILE
MPI_Barrier(appWorld);
initProfile(numPhases);
#endif

int phases = 0;
int iters = 0;
int profiling = 0;
double startstep, endstep;

double startt = MPI_Wtime();
int i;
for (i = 0; i < numPhases * iterations; ++i) {
  // "Computation" for the phase
  if (sleep) {
#ifdef PROFILE
    startComp();
#endif

    do_sleep(sleep);

#ifdef PROFILE
    endComp();
#endif
  }

#ifdef PROFILE
  startCommExchange();
#endif

  // Communication
  int reqi = 0;

  if (profiling) startstep = MPI_Wtime();

  postRecv(leftRecvBuf, msgSize, leftN, requests, &reqi);
  postRecv(topRecvBuf, msgSize, topN, requests, &reqi);
  postRecv(rightRecvBuf, msgSize, rightN, requests, &reqi);
  postRecv(bottomRecvBuf, msgSize, bottomN, requests, &reqi);

  postSend(leftSendBuf, msgSize, leftN, requests, &reqi);
  postSend(topSendBuf, msgSize, topN, requests, &reqi);
  postSend(rightSendBuf, msgSize, rightN, requests, &reqi);
  postSend(bottomSendBuf, msgSize, bottomN, requests, &reqi);

  MPI_Waitall(reqi, requests, MPI_STATUSES_IGNORE);

  if (profiling) {
    endstep = MPI_Wtime();
    printf("Rank %d, iteration %d, iteration time: %f\n", rank, i, endstep - startstep);
    ++iters;
    if (iters > 10) profiling = 0;
  }

#ifdef PROFILE
  endCommExchange();
#endif

  // Synchronize every "iterations" timesteps.
  if ((i + 1) % iterations == 0) {
#ifdef PROFILE
    enterSync();
#endif
    MPI_Barrier(appWorld);
    ++phases;
    if (phases == 2) profiling = 1;
#ifdef PROFILE
    exitSync(rank);
#endif
  }
}

// End app timing.
double endt = MPI_Wtime();
double myTime = endt - startt;

#ifdef SCOREP_USER_ENABLE
if (SCOREP_RECORDING_IS_ON()) {
  SCOREP_RECORDING_OFF();
}
#endif

// Gather the running times
double maxTime;
MPI_Reduce(&myTime, &maxTime, 1, MPI_DOUBLE, MPI_MAX, 0, appWorld);
if (rank == 0) {
  printf("Execution time: %f\n", maxTime);
}

#ifdef PROFILE
writeProfile(rank, ranks);
#endif

print_hosts();
return 0;

}

void postRecv(int * buf, int size, int source, MPI_Request requests[], int * reqi) {
  if (source >= 0) {
    MPI_Irecv(buf, size, MPI_INT, source, TAG, appWorld, &requests[*reqi]);
    ++(*reqi);
  }
}

void postSend(int * buf, int size, int dest, MPI_Request requests[], int * reqi) {
  if (dest >= 0) {
    MPI_Isend(buf, size, MPI_INT, dest, TAG, appWorld, &requests[*reqi]);
    ++(*reqi);
  }
}
