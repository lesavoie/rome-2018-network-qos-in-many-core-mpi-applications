/*
    Copyright (C) 2018  Staci Smith, Lee Savoie

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/*
 * Launcher to run as execuatble.
 */

#include <mpi.h>
#include <stdio.h>
#include <time.h>

#ifdef SCOREP_USER_ENABLE
#include <scorep/SCOREP_User.h>
#endif

extern MPI_Comm appWorld;

void print_hosts() {
  char hostname[1024];
  gethostname(hostname, 1024);

  int rank, size;
  MPI_Comm_rank(appWorld, &rank);
  MPI_Comm_size(appWorld, &size);

  char *recv = NULL;
  if (rank == 0) {
    recv = malloc(1024 * size);
  }

  MPI_Gather(hostname, 1024, MPI_CHAR, recv, 1024, MPI_CHAR, 0, appWorld);

  if (rank == 0) {
    int i;
    for (i = 0; i < size; i++) {
      printf("Rank %d: %s\n", i, recv + 1024 * i);
    }
  }
}

void do_sleep(long sleep) {
  struct timespec sleep_time;
  sleep_time.tv_sec = sleep / 1000000000;
  sleep_time.tv_nsec = sleep % 1000000000;  // In nanoseconds.
  nanosleep(&sleep_time, NULL);
}

extern int nn_main(int argc, char ** argv, MPI_Comm appWorld);

int main(int argc, char ** argv) {
  MPI_Init(&argc, &argv);

  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  if (rank == 0) {
    time_t curtime;
    time(&curtime);
    printf("Launched %s", ctime(&curtime));
  }

  int result = nn_main(argc, argv, MPI_COMM_WORLD);

  if (rank == 0) {
    time_t curtime;
    time(&curtime);
    printf("Completed %s", ctime(&curtime));
  }

#ifdef SCOREP_USER_ENABLE
  SCOREP_RECORDING_ON();
#endif

  MPI_Finalize();

  return result;
}

