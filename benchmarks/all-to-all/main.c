/*
    Copyright (C) 2018  Lee Savoie

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
/*
 * Simple global synchronization benchmark.
 */

#define _GNU_SOURCE
#include <sched.h>
#include <string.h>

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "mpi.h"

#define NN_DEBUG


void print_affinity(int myrank) {
  cpu_set_t my_set;
  CPU_ZERO(&my_set);
  sched_getaffinity(0, sizeof(cpu_set_t), &my_set);

  char str[80];
  strcpy(str," ");
  int count = 0;
  int j;
  for (j = 0; j < CPU_SETSIZE; j++) {
    if (CPU_ISSET(j, &my_set)) {
      count++;
      char cpunum[3];
      sprintf(cpunum, "%d ", j);
      strcat(str, cpunum);
    }
  }
  printf("Rank %d affinity has %d CPUs ... %s\n", myrank, count, str);
  fflush(stdout);
}

void set_affinity() {
  int rank, core;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  core = rank % 22 + 1;
  if (core >= 12) core += 1;
  cpu_set_t my_set;
  CPU_ZERO(&my_set);
  CPU_SET(core, &my_set);
  sched_setaffinity(0, sizeof(cpu_set_t), &my_set);
  print_affinity(rank);
}


int main(int argc, char ** argv) {
  MPI_Init(&argc, &argv);
  set_affinity();

  int rank, ranks;
  MPI_Comm_size(MPI_COMM_WORLD, &ranks);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  int msgSize, iterations;
  unsigned long sleepTime;

  if (argc != 4) {
    if (rank == 0) printf("Usage: %s <sleep time> <message size> <number of iterations>\n", argv[0]);
    MPI_Abort(MPI_COMM_WORLD, 1);
  }
  sleepTime = strtoul(argv[1], NULL, 10);
  msgSize = atoi(argv[2]);
  iterations = atoi(argv[3]);

  if (msgSize % 4 != 0) {
    if (rank == 0) printf("Message size must be a multiple of 4.\n");
    MPI_Abort(MPI_COMM_WORLD, 3);
  }
  msgSize /= 4;

  int sleepSec = sleepTime / 1000000000;
  int sleepNSec = sleepTime % 1000000000;
  struct timespec compTime = {sleepSec, sleepNSec};

#ifdef NN_DEBUG
  if (rank == 0) {
    printf("Sleep time: %lu, message size: %d, iterations: %d\n", sleepTime, msgSize, iterations);
    fflush(stdout);
  }
#endif

  int sendBuf[msgSize * ranks], recvBuf[msgSize * ranks];
  double *times = malloc((iterations + 1) * sizeof(double));
  times[0] = MPI_Wtime();
  for (int i = 0; i < iterations; ++i) {
    if (sleepTime > 0) nanosleep(&compTime, NULL);
    MPI_Alltoall(sendBuf, msgSize, MPI_INT, recvBuf, msgSize, MPI_INT, MPI_COMM_WORLD);
    times[i+1] = MPI_Wtime();
  }

  // Gather the running times
  double myTime = times[iterations] - times[0];
  double maxTime;
  MPI_Reduce(&myTime, &maxTime, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
  if (rank == 0) printf("Execution time: %f\n", maxTime);
  for (int i = 0; i < iterations; ++i) {
    printf("Rank %d, iteration %d elapsed time: %f\n", rank, i, times[i+1] - times[i]);
  }

  MPI_Finalize();
  return 0;
}
