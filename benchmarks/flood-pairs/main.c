/* 
   Argonne Leadership Computing Facility benchmark
   BlueGene/P version
   Bi-section bandwidth
   Written by Vitali Morozov <morozov@anl.gov>
   Modified by Daniel Faraj <faraja@us.ibm.com>
   
   Measures a partition minimal bi-section bandwidth
*/    
#define _GNU_SOURCE
#include <sched.h>
#include <string.h>

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


#define NN_DEBUG

void print_affinity(int myrank) {
  cpu_set_t my_set;
  CPU_ZERO(&my_set);
  sched_getaffinity(0, sizeof(cpu_set_t), &my_set);

  char str[80];
  strcpy(str," ");
  int count = 0;
  int j;
  for (j = 0; j < CPU_SETSIZE; j++) {
    if (CPU_ISSET(j, &my_set)) {
      count++;
      char cpunum[3];
      sprintf(cpunum, "%d ", j);
      strcat(str, cpunum);
    }
  }
  printf("Rank %d affinity has %d CPUs ... %s\n", myrank, count, str);
  fflush(stdout);
}

void set_affinity() {
  int rank, core;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  core = rank % 22 + 1;
  if (core >= 12) core += 1;
  cpu_set_t my_set;
  CPU_ZERO(&my_set);
  CPU_SET(core, &my_set);
  sched_setaffinity(0, sizeof(cpu_set_t), &my_set);
  print_affinity(rank);
}

main( int argc, char *argv[] )
{
  int ppn, taskid, ntasks, is1, ir1, i, k;
  char *sb, *rb;
  double d, d1;
  MPI_Status stat[2];
  MPI_Request req[2];
  double t1, elapsed, maxT;
  int sleepTime;
  struct timespec compTime;
 
  MPI_Init( &argc, &argv );
  set_affinity();
  MPI_Comm_rank( MPI_COMM_WORLD, &taskid );
  MPI_Comm_size( MPI_COMM_WORLD, &ntasks );
   
  if (argc != 4) {
    if (taskid == 0) printf("Usage: %s <message size> <number of iterations>\n", argv[0]);
    MPI_Abort(MPI_COMM_WORLD, 1);
  }
  int msgSize, iterations;
  sleepTime = atoi(argv[1]);
  msgSize = atoi(argv[2]);
  iterations = atoi(argv[3]);
  compTime.tv_sec = 0;
  compTime.tv_nsec = sleepTime;

#ifdef NN_DEBUG
  if (taskid == 0) {
    printf("Message size: %d, iterations: %d\n", msgSize, iterations);
    fflush(stdout);
  }
#endif

  posix_memalign( (void **)&sb, 16, sizeof( char ) * msgSize );
  posix_memalign( (void **)&rb, 16, sizeof( char ) * msgSize );

  if ( getranks( taskid, &is1, &ir1, &ppn ) == 0 )
  {
    //fprintf( stderr,"%d: %d => %d\n", taskid, is1, ir1 );
        
    d = 0.0;
    elapsed = 0.0;
        
    MPI_Barrier (MPI_COMM_WORLD);
    printf("Sleep time: %d nanoseconds\n", sleepTime);

    if ( taskid == is1 )
    {
      t1 = MPI_Wtime();
      for ( k = 0; k < iterations; k++ )
      {
        if (sleepTime > 0) nanosleep(&compTime, NULL);
        MPI_Isend( sb, msgSize, MPI_BYTE, ir1, is1, MPI_COMM_WORLD, &req[0] );
        MPI_Irecv( rb, msgSize, MPI_BYTE, ir1, ir1, MPI_COMM_WORLD, &req[1] );
        MPI_Waitall( 2, req, stat );
      }
      t1 = MPI_Wtime() - t1;
      elapsed = t1;
      
      t1 /= (double)iterations;
      d = 2 * (double)msgSize / t1;
    }

    if ( taskid == ir1 )
    {
      t1 = MPI_Wtime();
      for ( k = 0; k < iterations; k++ )
      {
        if (sleepTime > 0) nanosleep(&compTime, NULL);
        MPI_Isend( sb, msgSize, MPI_BYTE, is1, ir1, MPI_COMM_WORLD, &req[0] );
        MPI_Irecv( rb, msgSize, MPI_BYTE, is1, is1, MPI_COMM_WORLD, &req[1] );
        MPI_Waitall( 2, req, stat );
      }
      elapsed = MPI_Wtime() - t1;
    }

    MPI_Reduce( &d, &d1, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD );
    MPI_Reduce( &elapsed, &maxT, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
        
    printf("Rank %d elapsed time: %f\n", taskid, elapsed);
    if ( taskid == 0 ) {
      printf("PPN: %d Bisection BW(GB/s): %.2lf\n", ppn, d1 / 1e9 );
      printf("Overall elapsed time: %f\n", maxT);
    }
  }

  MPI_Finalize();
  //free( sb );
  //free( rb );
}
